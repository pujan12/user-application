class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :lastName
      t.string :firstName
      t.string :phoneNumber
      t.string :email

      t.timestamps
    end
  end
end
